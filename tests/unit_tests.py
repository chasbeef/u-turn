import unittest

from actions.shell_command import Command
from stage.stage import Stage, Rollback


class ShellCommandTasks(unittest.TestCase):

    def setUp(self):
        self.command_result = Command('echo \'hello world\'')

    def test_correct_exit_code(self):
        self.assertEqual(self.command_result.get_exit_code(), 0)

    def test_stdout_pipe(self):
        self.assertEqual(self.command_result.get_stdout_pipe_output(), 'hello world')

    def test_stderr_pipe(self):
        self.assertEqual(self.command_result.get_stderr_pipe_output(), '')

    def test_was_successful(self):
        self.assertEqual(self.command_result.was_successful(), True)

    def test_print_command(self):
        self.assertIsNotNone(self.command_result)

if __name__ == '__main__':
    unittest.main()