# File: stage/stagehandler.py
# Purpose: Handles creation of stages, general execution steps, and rollback in the event of failure.

from stage.stage import Stage, Rollback
import logging
from logconfig import config


class StageHandler(object):

    def __init__(self, tasks=[], rollback_tasks=[]):
        self.tasks = tasks
        self.rollback_tasks = rollback_tasks
        self.was_rolled_back = None

    def run_stages(self):
        for i in range(len(self.tasks)):
            current_stage = Stage(self.tasks[i], self.rollback_tasks[i])
            current_stage.run_stage()
            if not current_stage.was_successful():
                logging.error('Stage failed! Rolling back.')
                self.was_rolled_back = True
                rb = Rollback()
                if not rb.was_successful():
                    logging.error('Rollback failed at task %s.', rb.failed_command())
                else:
                    logging.warning('Rollback was successful.')
            else:
                self.was_rolled_back = False

    def get_rollback_status(self):
        return self.was_rolled_back
