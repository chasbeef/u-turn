# File: stage/stage.py
# Purpose: Defines a stage
# Author: Chas Berndt

from actions.shell_command import Command
import logging
from logconfig import config

logging.getLogger(__name__)


class Stage(object):
    undo = []

    def __init__(self, commands=[], rollback_commands=[]):
        logging.info('A stage was invoked with the following commands: {} and rollback_commands: {}'.format(commands, rollback_commands))
        self.commands = commands
        self.rollback_commands = rollback_commands
        for cmd in self.rollback_commands:
            self.undo.append(cmd)
        self.is_successful = None
        self.failed_at = None

    def run_stage(self):
        for cmd in self.commands:
            curr_cmd = Command(cmd)
            if not curr_cmd.was_successful():
                self.failed_at = curr_cmd
                self.is_successful = False
                break
        if self.is_successful is None:
            self.is_successful = True
            logging.info('Stage executed successfully.')

    def was_successful(self):
        return self.is_successful

    def failed_command(self):
        return self.failed_at


class Rollback(object):

    def __init__(self):
        logging.error('The following rollback sequence has been invoked: {}'.format(list(reversed(Stage.undo))))

        self.rollback_stage = Stage(list(reversed(Stage.undo)))
        self.rollback_stage.run_stage()

    def was_successful(self):
        return self.rollback_stage.was_successful()

    def failed_command(self):
        return self.rollback_stage.failed_command()