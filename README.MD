# U-Turn

U-Turn aims to be a simple command execution tool with the ability to rollback in the event of a command failure.

- Loads exection steps from a simple YAML format.
- Runs through each stage of execution, validating that each task runs successfully.
- If execution fails, U-Turn walks backwards through your rollback commands to get you back to a good state.

## License
Copyright 2018-Present Chas Berndt

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## How-To

### Use

```bash
uturn.py -file /path/to/config.yml
```

### Write YAML
U-Turn uses a simple YAML format which takes `stage` and `rollback` (optional) from an input YAML file.

#### Example

```yaml

---
stage:
- echo 'hello world'
- touch file.txt
rollback:
- rm file.txt
---
stage:
- yum install -y glances
# No rollback.

```