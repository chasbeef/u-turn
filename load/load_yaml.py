import yaml
import logging
from logconfig import config


class YamlLoader(object):

    def __init__(self, yaml_source, is_path=True):
        self.yaml_source = yaml_source
        self.yaml_body = yaml_source
        self.is_path = is_path
        self.loaded_yaml = None
        self.tasks = dict()
        self.rollback_tasks = dict()
        self.__load_yaml()

    def __load_yaml(self):
        if self.is_path is True:
            logging.debug('YAML input from file. Load %s', self.yaml_source)
            self.yaml_body = self.load_yaml_file()
        try:
                logging.debug('Loading YAML configuration \'%s\'', self.yaml_body)
                i = 0
                for job in yaml.load_all(self.yaml_body):
                    stage_tasks = job['stage']
                    if 'rollback' not in job:
                        stage_rollback = []
                    else:
                        stage_rollback = job['rollback']
                    logging.debug('stage: {} - rollback: {}'.format(stage_tasks, stage_rollback))
                    self.tasks[i] = stage_tasks
                    self.rollback_tasks[i] = stage_rollback
                    i += 1
                logging.info('YAML loaded successfully.')
        except Exception as e:
            logging.error('The following exception occurred while trying to load YAML. Exception: %s', e)

    def get_tasks(self):
        return self.tasks, self.rollback_tasks

    def load_yaml_file(self):
        with open(self.yaml_source, 'r') as yaml_file:
            return yaml_file.read()

