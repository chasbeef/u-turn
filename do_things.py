# Really simple example to show what I'm thinking about.

from stage.stagehandler import StageHandler
from load.load_yaml import YamlLoader
import logging
from logconfig import config

test_yaml = """
---
stage:
- echo first

---
stage:
- 'git --version'
- 'echo hello'
- 'ps aux | grep python'
rollback:
- 'echo fail'

---
stage:
- touch file.txt
- echo hello > file.txt
- cat file.txt
- 'cat /etc/centos-release'
rollback:
- rm file.txt
"""

tasks, rollback_tasks = YamlLoader(test_yaml, is_path=False).get_tasks()
stages = StageHandler(tasks, rollback_tasks)
stages.run_stages()
if stages.get_rollback_status() is True:
    logging.error('Execution failed.')
elif stages.get_rollback_status() is False:
    logging.info('Execution successful! Done.')
else:
    logging.error('Stages never run. Something went wrong.')

