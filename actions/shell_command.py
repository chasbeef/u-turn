# File: actions/shell_command.py
# Purpose: Provides a wrapper class for running a Shell command.
# Author: Chas Berndt

import subprocess
import logging
from logconfig import config

logging.getLogger(__name__)


class ShellCommand(object):

    def __init__(self, command_string, expected_exit_code=0, timeout=900):
        self.command_string = command_string
        self.expected_exit_code = expected_exit_code
        self.timeout = timeout
        self.raw_exit_code = None
        self.raw_shell_output = None
        self.raw_error_output = None
        self.__run_command()

    def __run_command(self):
        logging.debug('Preparing to execute the following command: %s.', self.command_string)
        try:
            process = subprocess.Popen(self.command_string, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            process.wait(timeout=self.timeout)
            self.raw_shell_output, self.raw_error_output = process.communicate()
            self.raw_exit_code = process.returncode
            logging.debug('Command executed successfully.')
            logging.debug('Command output: %s', self.get_stdout_pipe_output())
        except subprocess.TimeoutExpired as e:
            process.kill()
            self.raw_shell_output = ''
            self.raw_error_output = e
            self.raw_exit_code = 1
        if not self.was_successful():
            logging.error(self)

    def get_exit_code(self):
        if type(self.raw_exit_code) is bytes:
            return int.from_bytes(self.raw_exit_code, byteorder='little')
        else:
            return self.raw_exit_code

    def get_stdout_pipe_output(self):
        if type(self.raw_shell_output) is bytes:
            return self.raw_shell_output.decode('UTF-8').strip()
        return self.raw_shell_output.strip()

    def get_stderr_pipe_output(self):
        if type(self.raw_error_output) is bytes:
            return self.raw_error_output.decode('UTF-8').strip()
        return self.raw_error_output.strip()

    def was_successful(self):
        if self.get_exit_code() is self.expected_exit_code:
            return True
        else:
            return False

    # Overrides the default print method to return useful information about the execution of this command.
    def __repr__(self):
        return 'the command \'{}\' expected {} and {} was returned. stdout: \'{}\'. stderr: \'{}\' Was successful: {}'.format(
                                                                                           self.command_string,
                                                                                           self.expected_exit_code,
                                                                                           self.get_exit_code(),
                                                                                           self.get_stdout_pipe_output(),
                                                                                           self.get_stderr_pipe_output(),
                                                                                           self.was_successful())


class Command(ShellCommand):
    pass
