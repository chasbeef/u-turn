# File: uturn.py
# Purpose: Entry point for U-Turn
# Author: Chas Berndt

import logging
import argparse
from stage.stagehandler import StageHandler
from load.load_yaml import YamlLoader
from logconfig import config


def main():
    logging.info('========== Go for U-Turn ==========')
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', '-p', help='Path to U-Turn tasks input.')
    logging.debug('Parsing input arguments.')
    args = parser.parse_args()
    yaml_input = args.path
    logging.info('Path to YAML: %s', yaml_input)
    tasks, rollback_tasks = YamlLoader(yaml_input).get_tasks()
    stages = StageHandler(tasks, rollback_tasks)
    stages.run_stages()
    if stages.get_rollback_status() is True:
        logging.error('Execution failed.')
        exit(1)
    elif stages.get_rollback_status() is False:
        logging.info('Execution successful! Done.')
        exit(0)
    else:
        logging.error('Stages never run. Something went wrong.')
        exit(2)


if __name__ == '__main__':
    main()