# File: logging/config
# Purpose: Provides configuration for logging.

import logging

logging_format = '%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(message)s'

logging.basicConfig(level=logging.DEBUG, format=logging_format)


